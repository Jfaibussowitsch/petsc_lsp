(setq custom-file (expand-file-name "custom-file.el" user-emacs-directory))
(load custom-file :noerror :nomessage)

(defconst user-cache-dir
  (let ((dir (file-name-as-directory (expand-file-name "cache" user-emacs-directory))))
    (make-directory dir :parents)
    dir)
  "A directory for cached and temporary files")

(require 'package)
;; not needed after emacs 27
(when (version< emacs-version "27")
  (package-initialize))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

;; install use-package to make setting up lsp-mode easier
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; [OPTIONAL] install which-key mode to show key-prompt hints
(use-package which-key
  :ensure t
  :diminish
  :config
  (which-key-mode t)
  :custom
  (which-key-popup-type     'side-window)
  (which-key-compute-remaps  t))

;; [OPTIONAL, PERFORMANCE] Use Apple plists instead of JSON, they are faster to parse
(when (string= system-type "darwin")
  (setenv "LSP_USE_PLISTS" "true"))

;; install use-package, to make dealing with packages easier
(use-package lsp-mode
  :ensure t
  :hook
  ;; add more major mode as desired (e.g. python-mode, sh-mode)
  ;; lsp package automatically installs language servers for you if not present
  (c-mode   . lsp)
  (c++-mode . lsp)
  (cc-mode  . lsp)
  ;; if you want which-key integration
  (lsp-mode . lsp-enable-which-key-integration)
  :init
  ;; set prefix for lsp-command-keymap (few other alternatives - "C-l", "C-x l")
  (setq lsp-keymap-prefix "C-c l"
	lsp-session-file (concat user-cache-dir "lsp.cache")
	lsp-server-install-dir (concat user-cache-dir "lsp-servers"))
  :config
  ;; [OPTIONAL, FEATURE, PERFORMANCE] use LLVM clangd (more features, faster) instead of
  ;; Apple clangd 🤢
  ;; $ brew install llvm
  ;; $ export LLVM_DIR=$(brew --prefix)/opt/llvm
  (when-let ((llvm_dir (getenv "LLVM_DIR")))
    (setq lsp-clients-clangd-executable
          (locate-file "clangd" (ensure-list (concat (file-name-as-directory llvm_dir) "bin")))))
  (when (string= system-type "darwin")
    (setq lsp-use-plists t))
  :custom
  (lsp-clients-clangd-args
   `("--header-insertion=never"
     "--header-insertion-decorators"
     "-j=4"
     "--background-index"
     "--all-scopes-completion"
     ;; [OPTIONAL, FEATURE] Allow clangd configuration files, see
     ;; example_clangd_config.yaml and https://clangd.llvm.org/config
     "--enable-config"
     "--clang-tidy"
     ;; [OPTIONAL, FEATURE] un-comment if using LLVM clangd 15+!
     ;; "--include-cleaner-stdlib"
     ))
  ;; [OPTIONAL, PERFORMANCE] De-bounce delay when sending repeat message to language
  ;; server, clangd is very fast so can usually set this even lower. 0.5 is a good default
  ;; however
  (lsp-idle-delay 0.5)
  ;; [OPTIONAL] Increase max process output to 1MB
  (read-process-output-max (* 1024 1024))
  ;; [OPTIONAL, PERFORMANCE] If set to true can cause a performance hit
  (lsp-log-io nil)
  ;; [OPTIONAL, FEATURE] (on by default) Auto-restart LSP if language server dies
  (lsp-restart 'auto-restart)
  ;; [OPTIONAL, FEATURE] (on by default) Pretty-print docstrings where possible
  (lsp-signature-render-documentation t)
  ;; [OPTIONAL, FEATURE] (on by default) Header-line path
  (lsp-headerline-breadcrumb-enable t)
  ;; [OPTIONAL, FEATURE] (on by default) Show diagnostic counter in modeline
  (lsp-modeline-diagnostics-enable t)
  ;; [OPTIONAL, FEATURE] Allow lsp to format the document (i.e. apply clang-format)
  (lsp-enable-on-type-formatting t)
  (lsp-enable-indentation t)
  ;; [OPTIONAL, FEATURE] Allow lsp to enable completion, requires a completion framework
  ;; provider (e.g. company mode)
  ;; (lsp-completion-enable t)
  ;; (lsp-completion-provider :company)
  ;; (lsp-completion-show-detail t)
  ;; (lsp-completion-show-kind t)
  ;; [OPTIONAL, FEATURE] allow lsp to syntax highlight for you!
  (lsp-semantic-tokens-enable t))
